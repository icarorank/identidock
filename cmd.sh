#!/bin/bash
set -e

APP_DIR='/app'
pwd
ls

if [ "$ENV" = 'DEV' ]; then
    echo "Running Development Server"
    exec python "$APP_DIR/identidock.py"
elif [ "$ENV" = 'UNIT' ]; then 
    echo "Running Unit Tests"
    exec python "$APP_DIR/tests.py"
    exec uwsgi --http 0.0.0.0:9090 --wsgi-file "$APP_DIR/identidock.py" \
        --callable app --stats 0.0.0.0:9191
fi
